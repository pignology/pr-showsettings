all: pr-showsettings

pr-showsettings: pr-showsettings.c
	$(CC) -o $@ $< `pkg-config --cflags --libs libconfig`

clean:
	rm -f pr-showsettings.o pr-showsettings

