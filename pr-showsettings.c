/*
** pr-showsettings.c
** (C) Nick Garner, Pignology, LLC, 2014
**
** This program is free software: you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation, either version 3 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program.  If not, see <http://www.gnu.org/licenses/>.
**
*/

#include <stdio.h>
#include <libconfig.h>

int main()
{
  config_t config;
  /* init libconfig */                                                          
  config_init(&config);
  if (! config_read_file(&config, "/opt/PigRemote/etc/pigremote.settings"))
  {            
    fprintf(stderr, "%s:%d - %s\n", config_error_file(&config),
            config_error_line(&config), config_error_text(&config));
    config_destroy(&config);                                  
    return(1);                                
  }
  
  const char *str;
  int i;

  printf("\n");

  printf("Audio Settings:\n");
  config_lookup_int(&config, "pigremote.audio.route", &i);
  if (i == 1)
    printf("Audio route: Remote\n");       
  else if (i == 2)
    printf("Audio route: Local\n");       

  config_lookup_string(&config, "pigremote.audio.stunserver", &str);
  printf("STUN Server: %s\n", str);
  printf("\n");

  printf("Serial Settings:\n");
  config_lookup_int(&config, "pigremote.serial.pigtailbaud", &i);
  printf("Pigtail Baud: %d\n", i);       
  config_lookup_int(&config, "pigremote.serial.pigtailstopbits", &i);
  printf("Pigtail Stop Bits: %d\n", i);       
  config_lookup_string(&config, "pigremote.serial.pigtailport", &str);
  printf("Pigtail Serial Port: %s\n", str);       
  config_lookup_string(&config, "pigremote.serial.pigtailtcpport", &str);
  printf("Pigtail TCP Port: %s\n", str);       
  printf("\n");

  printf("Afraid DDNS Settings:\n");
  config_lookup_string(&config, "pigremote.afraid_ddns.afraid_url", &str);
  printf("Afraid URL: %s\n", str);
  printf("\n");

  return 0;
}

